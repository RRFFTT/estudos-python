#DICIONARIOS
#DICIONARIO SAO IDENTIFICADOS POR CHAVES {}, dict() para s declarar dicionarios. Ao inves de ultilizar numeros como inde
#ultiliza chaves (keys), o comando append nao funciona para adicionar ao dicionario. Values sao os valores que estao den
#tro do dicionario.
prateleira = list()
locadora = {}
# ▼dict▼  ▼key▼           ▼values▼
locadora['filme'] = 'stars wars'
locadora['ano'] = 1977
locadora['diretor'] = 'jorge lucas'
print(locadora.values()) #Valores (['stars wars', 1977, 'jorge lucas'])
print(locadora.keys()) #seriam os indexadores (['filme', 'ano', 'diretor'])
print(locadora.items()) #([('filme', 'stars wars'), ('ano', 1977), ('diretor', 'jorge lucas')])
#del locadora['ano'] #comando deletar
#prateleira.append(locadora)
#print(f'{"FILME":<15}{"ANO":<5}{"DIRETOR":<15}')
#key▼  ▼Value
print('▼' * 35, '\nfor com items():\n','▲' * 35)
for chave, valor in locadora.items(): #ultilizado .items
    print(f'{chave} = {valor}')
    print(valor)
print('▼' * 35, '\nfor com keys():\n','▲' * 35)
for chaves in locadora.keys():
    print(chaves)
print('▼' * 35, '\nfor com values():\n','▲' * 35)
for valores in locadora.values():
    print(valores)
print('▲' * 40)
print('▼' * 40)
pessoa = {'nome':'Rafael', 'sexo':'M','idade':'26'}
print(pessoa['nome'])
print(pessoa['idade'])
print(pessoa['sexo'])
print(pessoa.values())
print('▲' * 40)
print('▼' * 40)
brasil = list()
estado1 = {'uf' : 'PARANA', 'sigla': 'PR'}
estado2 = {'uf':'SANTA CATARINA', 'sigla': 'SC'}
#print(estado1['uf'], estado1['sigla'])
#print(estado2['uf'], estado2['sigla'])
brasil.append(estado1.copy()) #para colocar um dicionario dentro de uma lista usa-se o comando .copy()
brasil.append(estado2.copy())#assim fica um dicionario dentro de uma lista
#print(brasil[0]['uf'])
for estado in brasil:
    for key, value in estado.items():
        print(f'O campo {key} tem valor {value}')
print('▲' * 40)
print('▼' * 40)

