#Funções são trechos de código que podem ser executados em momentos diferentes de nossos códigos em Python. Veja como
# funciona o comando def em Python e como utilizá-lo com parâmetros simples e múltiplos.
def mostralinha():
    print('-' * 30 )
def pergunta():
    while True:
        perg = str(input('Quer adicionar mais um aluno? » ')).upper()
        if perg in 'N':
            break
        if not perg in 'SN':
            print('Opção invalida tente novamente.', end=' ')
def mensagem(msg):
    print('-' * 30)
    print(msg)
    print('-'*30)
# (a, b) itens dentro de parenteses sao parametros
def soma(a, b):
    s = a + b
    print(f'resultado [{s}]')


mensagem('OLA SOU A FUNÇÃO')
mensagem('teste 2')
mostralinha()
print('oi')
mostralinha()
soma(4, 4)

#!!!!! o * é o parametro de desempacotar, com isso o python trabalha com numero de parametros ililmitado !!!!
def contador(*num):
    for n in num:
        print(f'{n}')


#contador(8, 7, 2, 4, 6)
#contador(5,7,9)


def dobra(list):
    posição = 0
    while posição < len(list):
        list[posição] *= 2
        posição+= 1


valores = [2, 6, 8]
dobra(valores)
print(valores)