#Exercício Python 093: Crie um programa que gerencie o aproveitamento de um jogador de futebol. O programa vai ler o
# nome do jogador e quantas partidas ele jogou. Depois vai ler a quantidade de gols feitos em cada partida. No final,
# tudo isso será guardado em um dicionário, incluindo o total de gols feitos durante o campeonato.
dadosjogador = dict()
qntdegols = list()
dadosjogador['Nome'] = str(input('Nome jogador: ')).title()
dadosjogador['Partidas'] = int(input(f'Quantas partidas {dadosjogador["Nome"]} jogou? '))
for partidas in range(0, dadosjogador['Partidas']):
    qntdegols.append(int(input(f'   Quantos gols fez na {partidas+1}º partida: ')))
dadosjogador['Gols'] = qntdegols[:]
dadosjogador['Total de gols'] = sum(qntdegols)
print('=-=' * 15)
print(dadosjogador)
print('=-=' * 15)
for k, v in dadosjogador.items():
    print(f'» {k} tem valor {v}')
print('=-=' * 15)
for pos, gols in enumerate(qntdegols):
    print(f'Na partida {pos+1} {dadosjogador["Nome"]} marcou: {gols}')
print(f'Foi um total de {dadosjogador["Total de gols"]} gols marcados')
print('=-=' * 15)