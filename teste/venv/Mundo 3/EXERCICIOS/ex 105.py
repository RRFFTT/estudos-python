#Exercício Python 105: Faça um programa que tenha uma função notas() que pode receber várias notas de alunos e
# vai retornar um dicionário com as seguintes informações:
#– Quantidade de notas
#– A maior nota
#– A menor nota
#– A média da turma
#– A situação (opcional)

def notas(*num, sit=False):
     '''Adiciona varias notas e fazem uma analise.
      :param num = notas adicionadas
      :param sit=True(opcional): Mostra a situaçao com base da media
      :return resultado da analise'''
     notas  = dict()
     notas['total'] = len(num)
     notas['maior'] = max(num)
     notas['menor'] = min(num)
     notas['média'] = sum(num) / len(num)
     if sit == True:
         if notas['média'] >= 7.0:
             notas['sit'] = 'OTIMO'
         if notas['média'] > 5.1 or notas['média'] <= 6.9 :
             notas['sit'] = 'REGULAR'
         if notas['média'] <= 5.0:
            notas['sit'] = 'RUIM'
     return notas

#programa principal
#▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
resp = notas(5.5, 2.5, 10 ,6.5 ,sit=True)
print(resp)
help(notas)