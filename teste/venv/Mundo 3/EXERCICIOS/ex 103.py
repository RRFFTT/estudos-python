#Exercício Python 103: Faça um programa que tenha uma função chamada ficha(), que receba dois parâmetros opcionais:
#o nome de um jogador e quantos gols ele marcou. O programa deverá ser capaz de mostrar a ficha do jogador, mesmo que
# algum dado não tenha sido informado corretamente
def ficha(a=0, b=0):
    a = str(input('Nome do jogador: ')).strip()
    b = str(input('Numero de gols: ')).strip()
    if a == '':
        a = '<desconhecido>'
    if b == '' :
        b = '0'
    elif b.isalpha():
        b = '0'
    elif b.isnumeric():
        b = int(b)
    return print(f'O jogador {a}, fez {b} gol(s) no campeonato.')


ficha()