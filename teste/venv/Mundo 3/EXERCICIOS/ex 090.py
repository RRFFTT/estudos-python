#Exercício Python 090: Faça um programa que leia nome e média de um aluno, guardando também a
#situação(abaixo de 5 REPROVADO, 6 em recuperação, Acima de 7 APROVADO) em um dicionário.No final, mostre o conteúdo da
#estrutura na tela.
boletim = {}
boletim['Nome'] = str(input('Nome: ')).title()
boletim['Média'] = float(input('Qual foi a media: '))
if boletim['Média'] <= 5.9:
    boletim['Situação'] = 'REPROVADO'
if 6.0 >= boletim['Média']  < 7.0:
    boletim['Situação'] = 'RECUPERAÇÃO'
if boletim['Média'] >= 7.0:
    boletim['Situação'] = 'APROVADO'
print('▼' * 30)
for key, values in boletim.items():
    print(f'{key} é igual a {values}')
print('▲' * 30)