#Exercício Python 078: Faça um programa que leia 5 valores numéricos e guarde-os em uma lista. No final, mostre qual
# foi o maior e o menor valor digitado e as suas respectivas posições na lista.
num = []
for c in range(0,5):
    num.append(int(input(f'Digite o {c+1}º numero: ')))
print(f'O MAIOR numero digitado foi [{max(num)}] e esta no indice [{num.index(max(num))}].')
print(f'O MENOR numero digitado foi [{min(num)}] e esta no indice [{num.index(min(num))}].')
