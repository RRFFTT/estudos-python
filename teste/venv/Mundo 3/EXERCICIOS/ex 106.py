#Exercício Python 106: Faça um mini-sistema que utilize o Interactive Help do Python. O usuário vai digitar o
# comando e o manual vai aparecer. Quando o usuário digitar a palavra ‘FIM’, o programa se encerrará.
# Importante: use cores.

from time import sleep


def page(txt=0,cabeçalho=False, meio=False, pagetop=False, pagebottom=False, pagestop=False):
    if cabeçalho == True:
        print('\033[1;97;42m▼▲' * len(txt))
        print(f'{txt:^45}')
        print('▲▼' * len(txt))
        print('\033[m', end='')
    if meio == True:
        print('\033[1;97;104m▲▼' * len(txt))
        print(f'{txt:^50}')
        print('▲▼' * len(txt))
        print('\n\033[m',end='')
        sleep(1)
    if pagetop == True:
        print('\033[1;97;46m')
    if pagebottom == True:
        print('\033[m', end='')
    if pagestop == True:
        print('\033[1;97;41m')
        print(f'{txt:^50}')
        print('\n\033[m')

def pyhelp():
    while True:
        page('SISTEMA DE AJUDA PyHELP',cabeçalho=True)
        perg = str(input('FUNÇÃO OU BIBLIOTECA: ')).lower()
        if perg in 'fim':
            select = 'fim'
            page('ATÉ LOGO', pagestop=True)
            break
        page('Acesssando o manual comando...', meio=True)
        page((), pagetop=True)
        select = help(perg)
        page((), pagebottom=True)
        sleep(1)
    return select

pyhelp()
