#Exercício Python 075: Desenvolva um programa que leia quatro valores pelo teclado e guarde-os
# em uma tupla. No final, mostre:
#A) Quantas vezes apareceu o valor 9.
#B) Em que posição foi digitado o primeiro valor 3.
#C) Quais foram os números pares.
numeros = (int(input('Digite um numero: ')),int(input('Digite outro numero: ')),
           int(input('Digite o terceiro numero: ')), int(input('Digite o quarto numero: ')) )


print(f'Você digitou os números: {numeros}')
print(f'A » O valor 9 apareceu: [{numeros.count(9)}] vezes')
if numeros.count(3) > 0: #if 3 in numeros:
    print(f'B » O numero 3 apareceu primeiro na posição [{numeros.index(3) + 1}] ')
else:
    print('B » O numero 3 não apareceu em nunhuma posição!!')

print('C » Os numeros pares são ', end='»» ')
for num in numeros:
    if num % 2 == 0:
        print(num, end=' ')
