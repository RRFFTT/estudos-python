#Exercício Python 098: Faça um programa que tenha uma função chamada contador(), que receba três parâmetros: início,
# fim e passo. Seu programa tem que realizar três contagens através da função criada:
# a) de 1 até 10, de 1 em 1
# b) de 10 até 0, de 2 em 2
# c) uma contagem personalizada

from time import sleep
def linha():
    print('~' * 30)
def contador(a, b, c):
    if c < 0:
        c *= -1
    if c == 0:
        c = 1
    if a < b:
        print(f'Contagem de {a} até {b} de {c} em {c}!')
        for cont in range(a, b+1, c):
            print(cont, end=' ')
            sleep(0.1)
        print('FIM !')
        linha()
    elif a > b:
        print(f'Contagem de {a} até {b} de {c} em {c}!')
        for cont in range(a, b - 1, -c):
            print(cont, end=' ')
            sleep(0.3)
        print('FIM !')
        linha()








contador(1, 10, 1)
contador(10, 0, 2)
incio = int(input('INICIO: '))
fim = int(input('FIM: '))
passo = int(input('PASSO: '))
contador(incio, fim, passo)