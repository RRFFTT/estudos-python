#Exercício Python 091: Crie um programa onde 4 jogadores joguem um dado e tenham resultados aleatórios. Guarde esses
# resultados em um dicionário em Python. No final, coloque esse dicionário em ordem, sabendo que o vencedor tirou o
# maior número no dado.
from random import randint
from time import sleep
from operator import itemgetter
jogos = {'Jogador1' : randint(1,6),
             'Jogador2' : randint(1,6),
             'Jogador3' : randint(1,6),
             'Jogador4' : randint(1,6)}
for key, values in jogos.items():
    print(f'{key} tirou {values} no dado!')
    sleep(0.8)
ranking = list()
print('-' * 40)
print("=-=-=-=-==--=RANKING JOGADORES=-=-=-=-==--=")
ranking = sorted(jogos.items(), key=itemgetter(1), reverse=True )
# sorted() usado para organizar os valores ou chaves, deve se usar itemgetter() para obter o item que deseja organizar,
#ultilizado o reverse=True para ficar de forma decrecente.
#O sorted() para organizar os valores transforma o dicionario em lista.
for pos, colocacao in enumerate(ranking):
    print(f'Em {pos+1}º Lugar ficou {colocacao[0]} tirando {colocacao[1] } no dado!')
    sleep(0.8)
