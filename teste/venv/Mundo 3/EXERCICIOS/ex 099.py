#Exercício Python 099: Faça um programa que tenha uma função chamada maior(), que receba vários parâmetros com valores
# inteiros. Seu programa tem que analisar todos os valores e dizer qual deles é o maior.
from time import  sleep
from random import randint

#def com parametros variados, ou desempacotamento de parametros
def maior(*lista):
    maior_valor = 0
    print('=-=' * 20)
    print('Analisando os valores passados ...')
    for num_em_lista in lista:
        print(num_em_lista, end=' ')
        sleep(0.5)
        if num_em_lista >= maior_valor:
            maior_valor = num_em_lista
        else:
            maior_valor = maior_valor
    print(f'.Foram informados {len(lista)} valores ao todo.')
    print(f'O maior valor informado foi {maior_valor}.')



#program principal
maior(0 ,5 , 41, 7, 8, 4)
maior(randint(0,50), randint(0,50), randint(0,50), randint(0,50), randint(0,50))
maior(randint(0,50), randint(0,50), randint(0,50), randint(0,50), randint(0,50))
maior(randint(0,50), randint(0,50), randint(0,50), randint(0,50), randint(0,50))
maior(randint(0,50), randint(0,50), randint(0,50), randint(0,50))
maior(randint(0,50), randint(0,50), randint(0,50), randint(0,50))