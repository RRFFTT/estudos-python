#Exercício Python 72: Crie um programa que tenha uma tupla totalmente preenchida com uma contagem por extenso
# de zero até vinte. Seu programa deverá ler um número pelo teclado (entre 0 e 20) e mostrá-lo por extenso.
numeros = 'Zero', 'Um', 'Dois', 'Três', 'Quatro', 'Cinco', 'Seis', 'Sete', 'Oito', 'Nove', 'Dez', 'Onze', 'Doze', \
          'Treze', 'Quatorze', 'Quinze', 'Dezesseis', 'Dezessete', 'Dezoito', 'Dezenove', 'Vinte'
while True:
    numtec = int(input('Digite um numero inteiro entre 0 e 20: '))
    while not numtec <= 20:
        numtec = int(input('VALOR INCORRETO, TENTE NOVAMENTE !!! »» '))
    else:
        for pos, num in enumerate(numeros):
            if pos == numtec:
                print(f'{pos} = {num}')
    perg = str(input('Quer continuar? [S/N] »» ')).strip()
    while not perg in 'SsNn':
        perg = str(input('Opção inválida. Quer continuar? [S/N] »» ')).strip()
    if perg in 'Nn':
        break
print('▒▒▒▒▒▒▒▒▒▒' * 10)
print('Obrigado por ultilizar este programa !!')
print('▒▒▒▒▒▒▒▒▒▒' * 10)