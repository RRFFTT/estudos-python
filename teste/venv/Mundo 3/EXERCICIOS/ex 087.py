#Exercício Python 087: Aprimore o desafio anterior, mostrando no final:
# A) A soma de todos os valores pares digitados.
# B) A soma dos valores da terceira coluna.
# C) O maior valor da segunda linha.
######################################################################################################################
matriz = [0,0,0], [0,0,0], [0,0,0],[]
somatot = 0
for linha in range(0,3):
    for coluna in range(0,3):
        matriz[linha][coluna] = int(input(f'Digite um numero para [{linha}, {coluna}] » '))
        if matriz[linha][coluna] % 2 == 0:
            matriz[3].append(matriz[linha][coluna])
print('***' * 15)
for linha in range(0, 3):
    for coluna in range(0,3):
        print(f'[{matriz[linha][coluna]:^6}]', end='')
    print()
print(f'A) A soma de todos os numero pares é : {sum(matriz[3])}')
print(f'B) A soma dos valores da terceira linha é {sum(matriz[2])}')
print(f'C) O maior valor da segunda linha é {max(matriz[1])}')