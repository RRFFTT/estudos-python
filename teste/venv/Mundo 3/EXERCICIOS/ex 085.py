#Exercício085: Crie um programa onde o usuário possa digitar sete valores numéricos e cadastre-os em uma lista única
# que mantenha separados os valores pares e ímpares. No final, mostre os valores pares e ímpares em ordem crescente.
########################################################################################################################
listaunica = [], []
for rep in range(0, 7):
    num = int(input(f'Digite o {rep + 1}º numero » '))
    if num % 2 == 0:
        listaunica[0].append(num)
    else:
        listaunica[1].append(num)
listaunica[0].sort(), listaunica[1].sort()
print(f'Os numeros pares são: {listaunica[0]}')
print(f'Os numeros impares são: {listaunica[1]}')
