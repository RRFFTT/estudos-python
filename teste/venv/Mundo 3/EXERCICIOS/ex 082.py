#Exercício Python 082: Crie um programa que vai ler vários números e colocar em uma lista. Depois disso, crie duas
# listas extras que vão conter apenas os valores pares e os valores ímpares digitados, respectivamente. Ao final, mostre
# o conteúdo das três listas geradas.
lista = list()
numpar = list()
numimpar= list()

while True:
    lista.append(int(input('Digite um valor: »» ')))
    perg = str(input('Quer continuar [S/N] »» ')).upper()
    while not perg in 'SN':
        perg = str(input('Opção inválida. Digite novamente [S/N] »» ')).upper()
    if perg in 'N':
        break
for num in lista:
    if num % 2 == 0:
        numpar.append(num)
    else:
        numimpar.append(num)
print(f'Os numeros digitados foram: {lista}')
print(f'Os numeros pares dentro da lista são: {numpar}')
print(f'Os numeros impares são: {numimpar}')