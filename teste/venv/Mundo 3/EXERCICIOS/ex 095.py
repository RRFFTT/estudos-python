#Exercício Python 095: Aprimore o desafio 93 para que ele funcione com vários jogadores, incluindo um sistema de
# visualização de detalhes do aproveitamento de cada jogador.
jogador = dict()
gols= list()
geral = list()

while True:
    jogador['nome'] = str(input('Nome: ')).title()
    partidas = int(input(f'Quantas partidas {jogador["nome"]} jogou: '))
    for c in range(0, partidas):
        gols.append(int(input(f'Quantos gols na {c + 1}º: ')))
        jogador['gols'] = gols[:]
        jogador['total'] = sum(gols)
    geral.append(jogador.copy())
    while True:
        perg = str(input('Quer continuar [S/N]: ')).upper()
        if perg == 'S' or perg == 'N':
            break
        print('Tente novamente!!')
    if perg == 'N':
        break
print(f'{"COD":<4}{"NOME":<10}{"GOLS":<25}{"TOTAL"}')
print('---' * 15)
for pos, kdajogador in enumerate(geral):
        print(f'{pos:<4}{kdajogador["nome"]:<10}{str(kdajogador["gols"]):<25}{kdajogador["total"]}')
#                                                tranformar em string (str)
print('---' * 15)
while True:
        select = int(input('Mostrar dados de qual jogado [999 para sair]: '))
        if select == 999:
            break
        if select > len(geral) - 1 :
            print(f'Opção invalida tente novamente. COD {select} nao existe! ')
        else:
            print('---' * 15)
            print(f'-- LEVANTAMENTO DO JOGADOR {geral[select]["nome"]}')
            for pos, resultado in enumerate(geral[select]['gols']):
                print(f'    NA PARTIDA {pos + 1} MARCOU {resultado} GOLS')

print(f'{"Obrigado por ultilizar este programa":^60}')