#Exercício Python 094: Crie um programa que leia nome, sexo e idade de várias pessoas, guardando os dados de cada pessoa
# em um dicionário e todos os dicionários em uma lista. No final, mostre:
# A) Quantas pessoas foram cadastradas
# B) A média de idade
# C) Uma lista com as mulheres
# D) Uma lista de pessoas com idade acima da média
fichaindividual = dict()
fichageral = list()
idades = list()
mediadeidade = 0
acimadamedia = list()
while True:
    fichaindividual.clear()
    fichaindividual['nome'] = str(input('Nome: ')).title()
    fichaindividual['idade'] = int(input('Idade: '))
    fichaindividual['sexo'] = str(input('sexo [M/F] : ')).upper()
    while not fichaindividual['sexo'] in 'MF':
        print('Opção invalida tente novamente')
        fichaindividual['sexo'] = str(input('sexo [M/F] : ')).upper()
        if fichaindividual['sexo'] == True:
            break
    idades.append(fichaindividual['idade'])
    fichageral.append(fichaindividual.copy())
    perg = str(input('Quer continuar [S/N]: ')).upper()
    while not perg in 'SN':
        print('Opção inválida tente novamente!')
        perg = str(input('Quer continuar [S/N]: ')).upper()
        if perg == True:
            break
    if perg in 'N':
        break
print('**' * 20)
print(f'A) FORAM CADASTRADAS {len(fichageral)} PESSOAS.')
mediadeidade = sum(idades) / len(fichageral)
print(f'B) A MÉDIA DE IDADE CADASTRADA É {mediadeidade:.0f} ANOS')
print(f'C) AS MULHERES CADASTRADAS FORAM:',end=' ')
for pessoa in fichageral:
    if pessoa['sexo'] == 'F':
        print(pessoa['nome'], end=' ')
print('\n C) As pessoas acima da media de idade são:')
for dicionario in fichageral:
    if dicionario['idade'] >= mediadeidade:
        for k,v in dicionario.items():
            print(f'{k} = {v}; ',end='')
        print()
print('=-=-=-=-=-= ENCERRADO =-=-=-=-=-=')


