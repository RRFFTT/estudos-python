#listas sao representadas por []
# comandos: .append() = acrecentar, .insert() = inserir, del = deletar, .pop(), .remove() = remover,.sort() = organizar
#de forma crecente ou .sort(recerse=True) = de forma decrecente

valores = list(range(1, 11))
print(valores)
#Em ordem crescente
valores = [8, 2, 5, 4, 9, 3, 0]
valores.sort()
print(valores)
#em ordem inversa
valores = [8, 2, 5, 4, 9, 3, 0]
valores.sort(reverse=True)
print(valores)
print('*/*/*/*/*' * 10)
num = [2, 5, 9, 1]
num[2] = 3 #na posição 2, o 9 virou 3
num.append(7) #acrecenta(append) o 7 ao final da lista
num.sort() #.sort(reverse=True) ordem decrecente
num.insert(3, 2)#inserir(insert) no indice(index) 2, o n umero 0
#num.pop(2)#.pop() elimina o valor do indice 2
num.remove(2)#remove a primeira ocorrecia do valor que esta em remove
#num.remove(4)#sem a condição if o comando retorna um erro, pois nao tem 4 na lista:
num.append(4)
if 5 in num: #se 5 estiver na lista num:
    num.remove(5)#remove 5
else:#senao
    print('Nao achei o numero 4 na lista')#diga isso
print(num)
print(f'Essa lista tem {len(num)} itens')
print('*****/' * 15)
valores = []
valores.append(3)
valores.append(5)
valores.append(1)
valores.append(9)
print(valores)
#for valor in valores:
#    print(f'{valor}...', end=' ')
for ind, valor in enumerate(valores):
    print(f'No indice {ind} encontrei o valor {valor}.')
print('cheguei ao fim da lista!!')
print('***-***' * 15)
a = [1, 3, 6, 9]
b = a[:] #B recebe copia([:]) de a, se for somente (a) altera o indice 2 em ambas as listas
b[2] = 8 #na lista b substitua o indice 2 para 8

print(f'lista A ={a}')
print(f'lista B: {b}')
