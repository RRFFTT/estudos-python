#Nessa aula, vamos continuar nossos estudos de funções em Python, aprendendo mais sobre Interactive Help em Python, o
# uso de docstrings para documentar nossas funções, argumentos opcionais para dar mais dinamismo em funções Python,
# escopo de variáveis e retorno de resultados.


#comandos usados para ver o manual das funções

#print(input.__doc__)

def contador(i, f, p):
    """ função para contador
    :param i: inicio do contador
    :param f: Final do contador
    :param p: Passo
    :return: sem retorno
    Função criada por rafa
    """
    c=i
    while c <= f:
        print(f'{c}', end=' ')
        c += p
    print('FIM !')
#help(contador)

#PARAMETROS OPCIONAIS:
#def somar(a,b,c):
#    s = a + b + c
#    print(f'a soma resulta o valor {s}')
#»Parametro opcionais sao parametros que podem existir ou não. Por exemplo: somar(1, 2), sem colocar que (c=0)
#», vai retornar como erro pois nao recebeu o terceiro parametro, para corrigir o erro coloca-se
#»c=0, que tranforma em um parametro opcinal, senao digitado equivale a zero.
def somar(a,b,c=0):
    s = a + b + c
    print(f'a soma resulta o valor {s}')

somar(5,4,6)
somar(8, 4)

#ESCOPO DE VARIEVEIS:
#programa principal
#_________________________________________________
def teste():
    print(f'No programa principal n vale {n}')
#                                                  » ESCOPO GLOBAL
n = 2
teste()
#-------------------------------------------------
print('=-=' * 20)
def função():
    n1 = 4
    print(f'dentro n1 vale {n1}')


função()
n1 = 2
print(f'fora n1 vale {n1}')
print('=-=' * 20)
def teste(b):
    global a # para usar o parametro escopo global
    a = 8
    b += 4
    c = 2
    print(f'A dentro vale {a}')
    print(f'B dentro vale {b}')
    print(f'C dentro vale {c}')

a = 5
teste(a)
print(f'C fora vale {a}')
print('=-' * 20)
print()


def somar(a=0, b=0, c=0):
    s = a + b + c
    return s

print(f'usando o print {somar(3,6)}')
resp = somar(4,6, 8)
print(f'total da soma {resp}' )
print('=-' * 20)
print()
def par(num):
    if num % 2 == 0:
        return True
    else:
        return False

print(par(457))

num = int(input('Numero: '))
if par(num):
    print('▼É par')
else:
    print("▲Nao é par")