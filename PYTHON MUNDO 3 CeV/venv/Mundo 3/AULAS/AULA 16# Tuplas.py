#TUPLAS SÃO IMUTAVEIS
lanche = 'hamburger', 'suco', 'pizza', 'pudim'

print (lanche[0])
print(lanche[1])
print(lanche[0:2])
print(lanche[0:3])
print(len(lanche[0]))
print(lanche[-1])
print('=-=' * 20)
for comida in lanche:
    print(comida)      #para cada elemento na lista lanche me mostre o elemento
print('-=-' * 20)
for pos,comida in enumerate(lanche):
    print(f'{pos + 1}º {comida}', end=', ')
print('Ø»»»' * 20)
for comida in lanche:
   print(len(comida))
print('▒▒▒' * 20)
for pos, comida in enumerate(lanche):
    if pos + 1 == 2:
        print(pos,comida)
    if pos +1 == 4:
        print(f'{pos + 1}º {comida}')
print('╗╝╗╝╗╝╗╝' * 20)
#mesclando tuplas
a = (1, 5, 7)
b = (2, 4, 6)
c = a + b
print(c)
print('┼┼┼' * 50)
for numero in c:
    if numero % 2 == 0:
       print(f'{numero} é numero par')
    else:
        print(f'{numero} é numero impar')
print('xxxxx' * 50)

print(c.count(2))
print(c.index(3))
print(len(c))
del(a)