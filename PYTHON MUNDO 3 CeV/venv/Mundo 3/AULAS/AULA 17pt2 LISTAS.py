pessoas = [['PEDRO', 22], ['Maria', 55], ['Jose', 17]]
#                0              1             2
print(pessoas[0][0])
print(pessoas[2][1])
print(pessoas[1][0])
for p in pessoas:
    print(f'O NOME É {p[0]} tem {p[1]} anos  de idade.')
print('*/*' * 25)
dados = list()
galera = list()
for perg in range(0, 5):
    dados.append(str(input('Qual é o seu nome? »» ')))
    dados.append(int(input('Qual sua idade? »» ')))
    galera.append(dados[:])  # [:] copiar lista
    dados.clear()    #limpar lista
totmaior = totmenor = 0
print(f'As pessoas com mais de 18 anos são: ',end=f' ' )
for p in galera:
    if p[1] >= 18:
        print(p[0], end=' ')
print(f'\nAs pessoas menores de 18 são: ', end=' ')
for p in galera:
    if p[1] < 18:
        print(p[0], end=' ')
