#Exercício Python 101: Crie um programa que tenha uma função chamada voto() que vai receber como parâmetro o ano de
# nascimento de uma pessoa, retornando um valor literal indicando se uma pessoa tem voto NEGADO, OPCIONAL e OBRIGATÓRIO
# nas eleições.


def voto(ano_nascimento):
    from datetime import date
    ano_atual = date.today().year
    idade = ano_atual - ano_nascimento
    print(f'COM {idade} ANOS:', end=' ')
    if idade >= 18 and idade <= 64 :
        print('VOTO OBRIGATóRIO')
#       16 <= idade < 18 ← opção 2 para idade entre 16 e 18
    if idade >= 16 and idade < 18 or idade >= 65:
        print('VOTO OPCIONAL')
    else:
        print('VOTO NEGADO')


#programa principal
ano_nascimento = int(input('Que ano voce nasceu? '))
voto(ano_nascimento)