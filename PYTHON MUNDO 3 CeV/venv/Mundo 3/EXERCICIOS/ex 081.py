#Exercício Python 081: Crie um programa que vai ler vários números e colocar em uma lista.
# Depois disso, mostre:
# A) Quantos números foram digitados.
# B) A lista de valores, ordenada de forma decrescente.
# C) Se o valor 5 foi digitado e está ou não na lista.

lista = list()
while True:
    lista.append(int(input('Digite um valor »» ')))
    perg = str(input('Quer adicionar outro valor [S/N] »» ')).upper()
    while not perg in 'SsNn':
        print('Opção inválida. Tente novamente!!')
        perg = str(input('Quer adicionar outro valor [S/N] »» ')).upper()
    if perg in 'N':
        break
lista.sort(reverse=True)
print('«»«»'* 15)
print(f'A) Foram digitados {len(lista)} numeros na lista.')
print(f'B) A lista em forma decrescente: {lista}')
if 5 in lista:
    print('C) O número 5 aparece na lista.')
else:
    print(f'C) O valor 5 não aparece na lista.')
print('«»«»' * 15)