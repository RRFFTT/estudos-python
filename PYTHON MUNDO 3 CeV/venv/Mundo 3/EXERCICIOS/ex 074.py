#Exercício Python 074: Crie um programa que vai gerar cinco números aleatórios e colocar em uma tupla.
# Depois disso, mostre a listagem de números gerados e também indique o menor e o maior valor que estão na tupla.
from random import randint
aleatorio = (randint(0,10), randint(0,10), randint(0,10), randint(0,10), randint(0,10))

print(f'»» Os numeros gerados foram: ', end=' ')
for n in aleatorio:
    print(f'{n}', end=' ')
print(f'\n»» O maior numero sorteado foi: {max(aleatorio)} ')
print(f'»» O menor numero sorteado foi: {min(aleatorio)}')
