#Exercício Python 079: Crie um programa onde o usuário possa digitar vários valores numéricos e cadastre-os em uma lista
# Caso o número já exista lá dentro, ele não será adicionado. No final, serão exibidos todos os valores únicos digitados
# em ordem crescente.

valores = []
while True:
    numero = (int(input('Digite um valor: ')))
    if numero in valores:
        print('Numero já esta na lista valores.')
    else:
        print('Numero add')
        valores.append(numero)
    pergunta = str(input('Quer adicionar outro numero? [S/N] »» ')).upper()
    while not pergunta.upper() in 'SN':
        print('Opção invalida, tente novamente.')
        pergunta = str(input('Quer adicionar outro numero? [S/N] »» ')).upper()
        if pergunta in 'S':
            break
    if pergunta in 'N':
        valores.sort()
        break
print(f'Os valores registrados em forma crecente são: {valores}')
