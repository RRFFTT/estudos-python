#Exercício Python 084: Faça um programa que leia nome e peso de várias pessoas, guardando tudo em uma lista. No final,
# mostre:
# A) Quantas pessoas foram cadastradas.
# B) Uma listagem com as pessoas mais pesadas
# C) Uma listagem com as pessoas mais leves.
########################################################################################################################
temp = list()
dados = list()

while True:
    temp.append(str(input('Nome: »» ')))
    temp.append(float(input('Peso: »» ')))
    dados.append(temp[:])
    temp.clear()
    while True:                  #ALGORITMO DE CONTINUAR OU NAO
        perg = str(input('Quer continuar [S/N] »» ')).upper()
        if not perg in 'SN':
            print('Opção invalida, tente novamente!!')
        else:
            break
    if perg == 'N':
        break
print('****' * 15)
print(f'A) Foram cadastradas {len(dados)} pessoas.')
totpesos = list()
maiorpeso = list()
menorpeso = list()
for info in dados:
    totpesos.append(info[1])
maior = (max(totpesos))
menor = min(totpesos)
for numeros in dados:        #for  para separar nas listas de maior e menor pesos
    for num in numeros:
        if num == maior:
            maiorpeso.append(numeros)
        if num == menor:
            menorpeso.append(numeros)
print(f'B) O maior peso registrado foi {maior} KG. Peso de',end=' ')
for nome in maiorpeso:
    print(f'[{nome[0]}]', end=' ')
print(f'\nC) O menor peso registrado foi {menor} KG. Peso de ',end='')
for nomes in menorpeso:
    print(f'[{nomes[0]}]', end=' ')
print()
