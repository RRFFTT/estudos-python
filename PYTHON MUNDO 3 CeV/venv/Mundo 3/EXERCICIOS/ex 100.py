#Exercício Python 100: Faça um programa que tenha uma lista chamada números e duas funções chamadas sorteia() e
# somaPar(). A primeira função vai sortear 5 números e vai colocá-los dentro da lista e a segunda função vai mostrar a
# soma entre todos os valores pares sorteados pela função anterior.
from random import randint
from time import sleep


def sorteia(lista):
    for cont in range(0, 5):
        lista.append(randint(0, 20))
    print('Sorteando os 5 valores da lista:',end=' ')
    for num in lista:
        print(num, end=' ')
        sleep(0.5)
    print('PRONTO !')
def somapar(a):
    soma = 0
    for num in a:
        if num % 2 == 0:
            soma += num
    print(f'Somando os pares na lista {a}, temos {soma}')


numeros = list()

sorteia(numeros)
somapar(numeros)














