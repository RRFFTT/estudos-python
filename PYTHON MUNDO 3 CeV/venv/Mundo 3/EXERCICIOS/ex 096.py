#Exercício Python 096: Faça um programa que tenha uma função chamada área(), que receba as dimensões de um terreno
# retangular (largura e comprimento) e mostre a área do terreno.
def titulo(txt):
    print('▼' * 20)
    print(txt)
    print('▲' * 20)
def area(largura, comprimento):
    area = largura * comprimento
    print(f'A aréa de um terreno {largura:.1f}X{comprimento:.1f} é igual a {area:.1f}m²')

#programa principal
titulo('Area de um terreno')
a = float(input('Largura (m): '))
b = float(input('Comprimento (m): '))
area(a, b)
