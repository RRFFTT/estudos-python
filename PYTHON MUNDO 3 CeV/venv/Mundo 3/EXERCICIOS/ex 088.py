#Exercício 088: Faça um programa que ajude um jogador da MEGA SENA a criar palpites.O programa vai perguntar quantos
# jogos serão gerados e vai sortear 6 números entre 1 e 60 para cada jogo, cadastrando tudo em uma lista composta.
from random import randint
from time import  sleep

sorteio = list()
jogosgerados = list()


print('*' * 30)
print(f'{"JOGA NA MEGA SENA":^30}')
print('*'* 30)
ndejogos = int(input('Quantos jogos você quer gerar? » '))
for jogos in range(0, ndejogos):
    sorteio = [randint(1, 60), randint(1, 60), randint(1, 60), randint(1, 60), randint(1, 60), randint(1, 60)]
    for numero in sorteio:
        if sorteio.count(numero) > 1:
            sorteio.remove(numero)
            sorteio.append(randint(1,60))
    sorteio.sort()
    jogosgerados.append(sorteio[:])
    sorteio.clear()
print('♦♦♦' * 10)
print(f'{"Gerando jogos":^30}')
print('♦♦♦' * 10)
for pos, cadajogo in enumerate(jogosgerados):
    print(f'{pos + 1}ºJOGO: {cadajogo}')
    sleep(0.8)
print('♦♦♦' * 10)
print(f'{"Obrigado por ultizar este programa":^30}')
print('♦♦♦' * 10)