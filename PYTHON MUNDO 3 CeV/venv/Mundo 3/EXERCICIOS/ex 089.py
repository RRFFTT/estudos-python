#Exercício 089: Crie um programa que leia nome e duas notas de vários alunos e guarde tudo em uma lista composta. No final
# mostre um boletim contendo a média de cada um e permita que o usuário possa mostrar as notas de cada aluno individualmente.
temp = list()
arquivo = list()

while True:
    aluno = str(input('Qual o nome do aluno que deseja cadastrar: » ')).title()
    nota1 = float(input('Qual foi a primeira nota? » '))
    nota2 = float(input('Qual foi a segunda nota? » '))
    media = (nota1 + nota2) / 2
    temp.append(aluno), temp.append(nota1), temp.append(nota2), temp.append(media)
    arquivo.append(temp[:])
    temp.clear()
    while True:
        perg = str(input('Quer adicionar mais um aluno? » ')).upper()
        if not perg in 'SN':
            print('Opção invalida tente novamente.',end=' ')
        else:
            break
    if perg in 'N':
        break

print('♦' * 15)
print(f'{"Nº":^4}{"NOME":<16}{"MÉDIA":^4}')
print('---' * 10)
for pos in range(0,len(arquivo)):
    print(f'{pos:^4}{arquivo[pos]  [0]:<16}{arquivo[pos][3]:^4.2f}')
print('---' * 10)
while True:
    notas = int(input('Mostrar as notas de que aluno [999 interrope] » '))
    if notas == 999:
        break
    if notas <= len(arquivo) - 1:
        print('▬' * 30)
        print(f'Notas de {arquivo[notas][0]} são [{arquivo[notas][1]:.2f}, {arquivo[notas][2]:.2f}]')
        print('▬' * 30)
    if notas > len(arquivo) - 1 :
        while notas > len(arquivo) - 1:
            notas = int(input('Numero nao encontrado, tente novamnete. Mostrar as notas de que aluno [999 interrope] » '))
            if notas <= len(arquivo) - 1 or notas == 999:
                print('▬' * 30)
                print(f'Notas de {arquivo[notas][0]} são [{arquivo[notas][1]:.2f}, {arquivo[notas][2]:.2f}]')
                print('▬' * 30)
                break
    if notas == 999:
        break
print(f'\n{"VOLTE SEMPRE":-^30}')
