#Exercício Python 077: Crie um programa que tenha uma tupla com várias palavras (não usar acentos).
#Depois disso, você deve mostrar, para cada palavra, quais são as suas vogais.
dicionario = 'APRENDER', 'PROGRAMAR', 'LINGUAGEM', 'PYTHON', 'CURSO', 'GRATIS', 'ESTUDAR', 'PRATICAR', 'TRABALHAR',\
             'MERCADO', 'PROGRAMADOR', 'FUTURO'

for palavras in dicionario:
    print(f'\nNa palavra {palavras} tem as consoantes: ', end= '')
    for letra in palavras:
        if letra.count('A') == 1:
            print(letra, end=' ')
        if letra.count('E') == 1:
            print(letra, end=' ')
        if letra.count('I') == 1:
            print(letra, end=' ')
        if letra.count('O') == 1:
            print(letra, end=' ')
        if letra.count('U') == 1:
            print(letra, end=' ')
#Algoritimo guanabara
#for palavras in dicionario:
#   print(f'\nNa palavra {palavras} tem as consoantes: ', end=' ')
#   for letra in palavras:
#      if letra.upper() in 'AEIOU':
#         print(letra, end=' ')
