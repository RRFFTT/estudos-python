#Exercício Python 113: Reescreva a função leiaInt() que fizemos no desafio 104, incluindo agora a possibilidade da
# digitação de um número de tipo inválido. Aproveite e crie também uma função leiaFloat() com a mesma funcionalidade.

def leiaInt(a):
    while True:
        num = (input(a))
        try:
            numint = int(num)
            return numint
            break
        except:
            print('\033[1;31mERRO!! Digite um valor Inteiro válido.\033[m')



def leiaFloat(n):
    while True:
        try:
            num = float(input(n))
        except (ValueError, TypeError):
            print('\033[1;31mERRO!! Digite um valor Real válido.\033[m')
            continue
        except (KeyboardInterrupt):
            print('nao contionia')
            break
        else:
            return num



numint = leiaInt('Digite um numero inteiro: ')
numfloat = leiaFloat('digite um numero Flutuante: ')
print(f'O valor inteiro digitado foi {numint}, e o real foi {numfloat}')






