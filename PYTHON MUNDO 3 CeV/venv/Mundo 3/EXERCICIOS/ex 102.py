#Exercício Python 102: Crie um programa que tenha uma função fatorial() que receba dois parâmetros: o primeiro que
# indique o número a calcular e outro chamado show, que será um valor lógico (opcional) indicando se será mostrado ou
# não na tela o processo de cálculo do fatorial.


def fatorial(num_fatorial, show=False):
    '''-> Calcula o fatorial de um numero:
    :param num_fatorial O numero a ser calculado.
    :param show(opcional): Mostra ou não a conta.
    :return: O valor do fatorial do numero num_fatorial
    '''
    f = 1
    print(f'{num_fatorial}! =', end=' ')
    for n1 in range(num_fatorial, 0, -1):
        if show:
            print(f'{n1}', end=' ')
            if n1 > 1:
                print('X', end=' ')
            else:
                print('=', end=' ')
        f *= n1
    return f

print(fatorial(10, show=True))
