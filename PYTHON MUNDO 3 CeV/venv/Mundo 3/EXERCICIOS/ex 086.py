#Exercício 086: Crie um programa que declare uma matriz de dimensão 3×3 e preencha com valores lidos pelo teclado.
# No final, mostre a matriz na tela, com a formatação correta.
#######################################################################################################################
matriz = [], [], []
for num in range(0,3):
    valor = int(input(f'Digite um valor para [0 ,{num}] »» '))
    matriz[0].append(valor)
for num in range(0,3):
    valor = int(input(f'Digite um valor para [1 ,{num}] »» '))
    matriz[1].append(valor)
for num in range(0,3):
    valor = int(input(f'Digite um valor para [2 ,{num}] »» '))
    matriz[2].append(valor)
print('***' * 15)
for valores in matriz[0]:
    print(f'[{valores:^6}]',end=' ')
print()
for valores in matriz[1]:
    print(f'[{valores:^6}]', end=' ')
print()
for valores in matriz[2]:
    print(f'[{valores:^6}]', end=' ')
print()
######################################################################################################
#Segunda resolução:
#matriz = [0,0,0],[0,0,0],[0,0,0]
#for linha in range(0,3):  gera 0,1,2
#       for coluna in range(0,3): Gera 0,1,2
#           matriz[l][c] = int(input(f'Digite um numero [{l}, {c}] » '))
#for linha in range(0,3):
#   for coluna in range(0,3):
#      print(f'[{matriz[l][c]:^6}]',end='')
#   print()