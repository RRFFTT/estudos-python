#Exercício Python 104: Crie um programa que tenha a função leiaInt(), que vai funcionar de forma semelhante ‘a função
# input() do Python, só que fazendo a validação para aceitar apenas um valor numérico. Ex: n = leiaInt(‘Digite um n: ‘)
def leiaInt(a):
    while True:
        n = str(input(a))
        if n.isnumeric() == False:
                print('\033[1;31mERRO!!!Digite um número inteiro válido.\033[m')
        else:
            return n
            break






#PROGRAMA PRINCIPAL
n = leiaInt('Digite um numero: ')
print(f'\033[1;32;40mO valor digitado foi {n}.\033[m')





