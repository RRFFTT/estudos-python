#Exercício Python 092: Crie um programa que leia nome, ano de nascimento e carteira de trabalho e cadastre-o (com idade)
# em um dicionário. Se por acaso a CTPS for diferente de ZERO, o dicionário receberá também o ano de contratação e o
# salário. Calcule e acrescente, além da idade, com quantos anos a pessoa vai se aposentar.
from datetime import date
anoatual = date.today()
registro = dict()
registro['Nome'] = str(input('Nome: ')).title()
anonascimento = int(input('Ano de nascimento: '))
registro['idade'] = anoatual.year - anonascimento
registro['ctps'] = int(input('Carteira de trabalho (0 não tem): '))

if registro['ctps'] != 0:
    registro['Contratação'] = int(input('Ano de contratação: '))
    registro['Salario'] = float(input('Salario: R$ '))
    registro['Aposentadoria'] = registro['idade'] + ((registro['Contratação'] + 35) - anoatual.year)
print('=-=' * 15)
for key, values in registro.items():
    print(f'- {key} tem valor {values}')
#para ano atual da pra usa o tmb o "from datetime import datetime" com o comando "datetime.now().year" que gera o ano atual