#Exercício Python 73: Crie uma tupla preenchida com os 20 primeiros colocados da
# Tabela do Campeonato Brasileiro de Futebol, na ordem de colocação. Depois mostre:
#a) Os 5 primeiros times.
#b) Os últimos 4 colocados.
#c) Times em ordem alfabética.
#d) Em que posição está o time da Chapecoense.
times20 = 'Flamengo', 'Santos', 'Palmeiras', 'Grêmio', 'Athletico Paranaense', 'São Paulo', 'Internacional', 'Corinthians', \
          'Fortaleza', 'Goiás', 'Bahia', 'Vasco da Gama', 'Atlético Mineiro', 'Fluminense', 'Botafogo',\
          'Ceará', 'Cruzeiro', 'CSA', 'Chapecoense', 'Avaí'
print('»A) Os 5 primeiros colocados são: ')
for pos, time in enumerate(times20[:5]):
#    if pos < 5:
        print(f'{pos + 1}º {time}', end=' ')
print(f'\n»B) Os 4 ultimos são: ')
for pos, time in enumerate(times20[-4:]):
    print(f'{pos + 17}º{time}', end=' ')
print(f'\n»C) Times em ordem alfabetica: ')
for time in (sorted(times20)):
    print(time)
print(f'»D) O time Chapecoense esta em {times20.index("Chapecoense") + 1}º colocação')
