#Exercício Python 076: Crie um programa que tenha uma tupla única com nomes de produtos e
#seus respectivos preços, na sequência. No final, mostre uma listagem de preços, organizando os dados em forma tabular.
produltos = ('Lapis', 1.75, 'Borracha', 2.00, 'Caderno', 15.00, 'Estojo', 25.00, 'Transferidor', 4.20, 'Compasso', 9.99
             ,'Mochila', 120.32, 'Canetas', 22.30, 'Livro', 34.90)


print('----' * 10, f'\n{"Listagem  de materiais": ^40}\n', '----' * 10)

for num, prod in enumerate(produltos):
    if num % 2 == 0:
        print(f'{prod:.<30} ', end='')
    else:
        print(f'R$ {prod:7.2f}')
print('---' * 15)