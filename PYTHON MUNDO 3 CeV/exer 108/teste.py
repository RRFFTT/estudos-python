import moeda

preço = float(input('Qual é o preço? R$ '))
print(f'A metade de {moeda.moeda(preço)} é {moeda.moeda(moeda.metade(preço))}.')
print(f'O dobro de {moeda.moeda(preço)} é {moeda.moeda(moeda.dobro(preço))}.')
print(f'A adição de 10% no valor de {moeda.moeda(preço)} fica {moeda.moeda(moeda.aumentar(preço, 10))}')
