def dobro(num):
    num *= 2
    return num

def metade(num):
    num /= 2
    return num


def aumentar(num, taxa):
    pt1 = num * taxa
    tot = pt1 / 100
    add = tot + num
    return add


def moeda(num=0,c="R$"):
    return str(f'{c}{num:.2f}').replace('.', ',')
