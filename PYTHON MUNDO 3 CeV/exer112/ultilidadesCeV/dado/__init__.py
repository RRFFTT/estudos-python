#Exercício Python 112: Dentro do pacote utilidadesCeV que criamos no desafio 111, temos um módulo chamado dado. Crie uma
# função chamada leiaDinheiro() que seja capaz de funcionar como a função imputa(), mas com uma validação de dados para
# aceitar apenas valores que seja monetários.
#from exer112.ultilidadesCeV import moeda
def leiaDinheiro(a):
    while True:
        letra = 0
        b = input(a).replace(',', '.')
        for c in b:
            if c.isalpha() or c.isspace():
                letra += 1
            if c in '!@#$%¨&*()_+-|>:':
                letra += 1
        if letra > 0:
            print(f'\033[1;31mErro !!! \033[m\033[1;34m"{b}"\033[m\033[1;31m é um formato inválido.\033[m')
            return leiaDinheiro(a)
        else:
            return float(b)
