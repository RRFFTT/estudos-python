from time import sleep
from Exerc115.lib.arquivo import *
def titulo(msg):
    print('-' * 60)
    print(f'{msg}'.center(60))
    print('-' * 60)
def linha():
    print(f'-' * 60)
def menu(vercadastro, cadastrarnovo, sair,opcaoextra=0, arquivonome='Arquivo de cadastro'):
    #-----------------------VERIFICAÇÃODEARQUIVO---------------------------
    verificadordearquivo(f'{arquivonome}')
    #-------------------------MENU--------------------------------
    opc = {1: f'{vercadastro}', 2: f'{cadastrarnovo}', 3: f'{sair}'}
    if opcaoextra != 0:
        opc[3] = opcaoextra
        opc[4] = sair
    #interface menu
    while True:
        titulo('MENU PRINCIPAL')
        for k, v in opc.items():
            print(f'\033[1;33m{k} - \033[m\033[1;34m{v}\033[m')
        #---------------TRATAMENTO DE ERROS ESCOLHA DA OPÇÃO----------------
        while True:
            try:
                opcusuario = int(input('\033[1;33mSua opção: \033[m'))
                opc[opcusuario]
            except(ValueError):
                print('\033[1;31mERRO! Por favor, digite um número inteiro válido.\033[m')
            except(KeyError):
                print('\033[1;31mERRO! Por favor, digite uma opção válida.\033[m')
                sleep(1)
                break
            titulo(f'Opção selecionada \033[1;32m[{opcusuario} - {opc[opcusuario]}]\033[m')
            sleep(1.5)
            #-------------------------FUNÇÃO DAS OPÇÕES--------------------------------------
            #OPÇÃO 1(vercadastro)
            if opcusuario == 1:
                titulo('PESSOAS CADASTRADAS')
                leraquivo(f'{arquivonome}')
                linha()
            #----------Cadastrar novo usuario-----------
            if opcusuario == 2:
                titulo('NOVO CADASTRO')
                cadastro(arquivonome)
                linha()
            if opcaoextra!=0:
                if opcusuario == 4:
                    break
            else:
                if opcusuario == 3:
                    break
        titulo('Saindo do sistema ... Até logo !!')
        break







