def verificadordearquivo(arq):
    try:
        arquivo = open(f'{arq}.txt', 'rt')
        arquivo.close()
        print(f'\033[1;36mArquivo [{arq}.txt] acessado com sucesso !!\033[m')
    except(FileNotFoundError):
        arquivo = open(f'{arq}.txt', 'a')
        print(f'\033[1;32mArquivo [{arq}.txt] criado com sucesso... !!!\033[m ')
def leraquivo(arq):
    arquivo = open(f'{arq}.txt', 'r')
    print(arquivo.read())
    arquivo.close()

def cadastro(arq):
    temp = dict()
    arquivo = open(f'{arq}.txt', 'a')
    temp['nome'] = str(input('nome: ')).title()
    #se o usuario nao digitar nada
    if temp['nome'] == '':
        temp['nome'] = '< deconhecido >'
    temp['idade'] = str(input('idade: '))
    #verificador de numero
    if temp['idade'].isnumeric() == False:
        while True:
            print('\033[1;31mERRO tente novamente. Digite um numero inteiro.\033[m')
            temp['idade'] = str(input('idade: '))
            if temp['idade'].isnumeric() == True:
                break
    arquivo.write(f'{temp["nome"]:<40} {temp["idade"]:<3}anos\n')
    print(f'\033[1;32;40mUsuario {temp["nome"]} cadastrado com sucesso !!!\033[m')
    arquivo.close()