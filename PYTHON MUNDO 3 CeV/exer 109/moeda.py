


def moeda(num=0, c="R$"):
    return str(f'{c}{num:.2f}').replace('.', ',')


def dobro(num, formato=False):
    num *= 2
    return num if formato is False else moeda(num)


def metade(num, formato=False):
    num /= 2
    return num if formato is False else moeda(num)


def aumentar(num, taxa, formato=False):
    add = num + (num * taxa / 100)
    return num if formato is False else moeda(num)


def reducao(num, taxadesc, formato=False):
    desc = num - (num * taxadesc / 100)
    return desc if formato is False else moeda(desc)


def resumo(num, taxa=10, taxadesc=5):
    def aumentar(num, taxa, formato=False):
        add = num + (num * taxa / 100)
        return add if formato is False else moeda(add)

    def reducao(num, taxadesc, formato=False):
        desc = num - (num * taxadesc / 100)
        return desc if formato is False else moeda(desc)

    print('-' * 35)
    print(f'{"RESUMO DO VALOR":^35}')
    print('-' * 35)
    dados = dict()
    dados['Preço analisado: '] = moeda(num)
    dados['Dobro do preço: '] = dobro(num, True)
    dados['Metade do preço: '] = metade(num, True)
    dados[f'{taxa} % aumento: '] = aumentar(num, taxa, True)
    dados[f'{taxadesc} % redução: '] = reducao(num, taxadesc, True)
    for chave, valor in dados.items():
        print(f'{chave:<25}{valor:<10}')
    print('-' * 35)