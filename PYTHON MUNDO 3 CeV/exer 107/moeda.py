def dobro(num):
    num *= 2
    return f'{num}'

def metade(num):
    num /= 2
    return f'{num}'


def aumentar(num, taxa):
    pt1 = num * taxa
    tot = pt1 / 100
    add = tot + num
    return f'{add}'
