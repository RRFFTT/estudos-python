import moeda

preço = float(input('Qual é o preço? R$ '))
print(f'A metade de R$ {preço} é R$ {moeda.metade(preço)}.')
print(f'O dobro de R$ {preço} é {moeda.dobro(preço)}.')
print(f'A adição de 10% no valor de R$ {preço} fica R$ {moeda.aumentar(preço, 10)}')